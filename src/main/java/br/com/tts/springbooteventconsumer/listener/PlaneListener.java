package br.com.tts.springbooteventconsumer.listener;

import br.com.tts.springbooteventconsumer.entity.Plane;
import org.springframework.stereotype.Component;

/**
 * @author Tiago Luiz Fernandes
 */

@Component
public class PlaneListener implements GenericListener<Plane> {

    @Override
    public void handle(Plane plane) {
        System.out.println(plane);
    }

    //@EventListener
    //public void handle(Plane plane) {
    //
    //}
}
