package br.com.tts.springbooteventconsumer.listener;

import org.springframework.context.event.EventListener;

/**
 * @author Tiago Luiz Fernandes
 */
public interface GenericListener<T> {

    @EventListener
    public abstract void handle(T t);

}
