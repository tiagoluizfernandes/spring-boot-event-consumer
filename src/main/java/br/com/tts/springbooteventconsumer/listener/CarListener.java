package br.com.tts.springbooteventconsumer.listener;

import br.com.tts.springbooteventconsumer.entity.Car;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @author Tiago Luiz Fernandes
 */

@Component
public class CarListener {

    @EventListener
    public void handle(Car car) {
        System.out.println(car);
    }
}
