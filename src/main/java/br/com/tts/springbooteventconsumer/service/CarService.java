package br.com.tts.springbooteventconsumer.service;

/**
 * @author Tiago Luiz Fernandes
 */

import br.com.tts.springbooteventconsumer.entity.Car;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Slf4j
@Service
public class CarService {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    /*
    @KafkaListener(topics = "${kafka.topic.event.operationsposition}", groupId = "${kafka.topic.groupID}", containerFactory = "${kafka.containerFactory.positionoperation}")
    public void listen(final String message, final MessageHeaders messageHeaders) throws IOException {
        MDC.put("correlationId", messageHeaders.get("guid").toString());
        publish(message, messageHeaders);
    }
    */

    public void publish(@Payload Car car) throws IOException {
        this.applicationEventPublisher.publishEvent(car);
    }
}

