package br.com.tts.springbooteventconsumer.controller;

import br.com.tts.springbooteventconsumer.entity.Car;
import br.com.tts.springbooteventconsumer.entity.Plane;
import br.com.tts.springbooteventconsumer.service.CarService;
import br.com.tts.springbooteventconsumer.service.PlaneService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

/**
 * @author Tiago Luiz Fernandes
 */

@Slf4j
@RestController()
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/")
public class EventController {

    private final CarService carService;

    private final PlaneService planeService;

    @GetMapping(path = "/car")
    public String publishCar() throws IOException {

        String message = "car published";

        //Map<String, Object> map = new HashMap<>();

        Car car = new Car();
        car.setModel("car model");
        car.setPlate("car plate");

        //map.put("type","br.com.tts.springbooteventconsumer.entity.CAR");

        //MessageHeaders messageHeaders = new MessageHeaders(map);
        carService.publish(car);

        return message;
    }

    @GetMapping(path = "/plane")
    public String publishPlane() throws IOException {

        String message = "plane published";

        //Map<String, Object> map = new HashMap<>();

        Plane plane = new Plane();
        plane.setModel("air model");
        plane.setPlate("air plate");

        //map.put("type","br.com.tts.springbooteventconsumer.entity.CAR");

        //MessageHeaders messageHeaders = new MessageHeaders(map);
        planeService.publish(plane);

        return message;
    }

}
