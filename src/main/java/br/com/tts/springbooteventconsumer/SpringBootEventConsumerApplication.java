package br.com.tts.springbooteventconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootEventConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootEventConsumerApplication.class, args);
	}

}
