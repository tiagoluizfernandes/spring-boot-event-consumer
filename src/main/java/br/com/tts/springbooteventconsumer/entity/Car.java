package br.com.tts.springbooteventconsumer.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.ApplicationEvent;

import java.io.Serializable;

/**
 * @author Tiago Luiz Fernandes
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Car  implements Serializable{
    private String model;
    private String plate;
}
