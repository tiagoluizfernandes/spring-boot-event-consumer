package br.com.tts.springbooteventconsumer.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Tiago Luiz Fernandes
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Plane implements Serializable {
    private String model;
    private String plate;
}

